var arbol = new Arbol();
//Nivel 1
arbol.nodoPadre.hI = arbol.agregarNodo(arbol.nodoPadre,"Hoja Izquierda","16",16);
arbol.nodoPadre.hD = arbol.agregarNodo(arbol.nodoPadre,"Hoja Derecha","20",20);
//Nivel 2

arbol.nodoPadre.hI.hI = arbol.agregarNodo(arbol.nodoPadre.hI,"Hoja Izquierda","8",8);
arbol.nodoPadre.hI.hD = arbol.agregarNodo(arbol.nodoPadre.hD,"Hoja Derecha","8",8);

arbol.nodoPadre.hD.hI = arbol.agregarNodo(arbol.nodoPadre.hD,"Hoja Izquierda","8",8);
arbol.nodoPadre.hD.hD = arbol.agregarNodo(arbol.nodoPadre.hD,"Hoja Derecha","12",12);
//Nivel 3

arbol.nodoPadre.hI.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI,"Hijo Izquierda","e",4);
arbol.nodoPadre.hI.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hD,"Hijo Derecha","4",4);

arbol.nodoPadre.hI.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI,"Hijo Izquierda","a",4);
arbol.nodoPadre.hI.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hD,"Hijo Derecha","4",4);

arbol.nodoPadre.hD.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI,"Hijo Izquierda","4",4);
arbol.nodoPadre.hD.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hD,"Hijo Derecha","4",4);

arbol.nodoPadre.hD.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI,"Hijo Izquierda","5",5);
arbol.nodoPadre.hD.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hD,"Hijo Derecha"," ",7);
//nivel 4

arbol.nodoPadre.hI.hI.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hI,"Hijo Izquierda","n",2);
arbol.nodoPadre.hI.hI.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD,"Hijo Derecha","2",2);

arbol.nodoPadre.hI.hD.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hI,"Hijo Izquierda","t",2);
arbol.nodoPadre.hI.hD.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD,"Hijo Derecha","m",2);

arbol.nodoPadre.hD.hI.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI,"Hijo Izquierda","i",2);
arbol.nodoPadre.hD.hI.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hD,"Hijo Derecha","2",2);

arbol.nodoPadre.hD.hI.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI,"Hijo Izquierda","h",2);
arbol.nodoPadre.hD.hI.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hD,"Hijo Derecha","s",2);

arbol.nodoPadre.hD.hD.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI,"Hijo Izquierda","2",2);
arbol.nodoPadre.hD.hD.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hD,"Hijo Derecha","f",3);
//Nivel 5
arbol.nodoPadre.hI.hI.hD.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD.hI,"Hijo Izquierda","o",1);
arbol.nodoPadre.hI.hI.hD.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD.hD,"Hijo Derecha","u",1);

arbol.nodoPadre.hD.hI.hI.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI.hI,"Hijo Izquierda","x",1);
arbol.nodoPadre.hD.hI.hI.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hD,"Hijo Derecha","p",1);

arbol.nodoPadre.hD.hD.hI.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI.hI,"Hijo Izquierda","r",1);
arbol.nodoPadre.hD.hD.hI.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hD,"Hijo Derecha","l",1);

//impresion
console.log("Nivel buscado:" + arbol.nivel);
var buscar=arbol.verificarNivelHijos(arbol.nodoPadre);
console.log(buscar);
var busqueda = 4;
var busqueda = arbol.buscarValor(busqueda,arbol.nodoPadre);
var caminoNodo=arbol.buscarCaminoNodo(busqueda);
console.log("Camino recorrido:")
console.log(caminoNodo);
var sumaDeCaminos=arbol.sumarCaminoNodo(busqueda);
console.log("Suma del recorrido;");
console.log(sumaDeCaminos);
console.log("Arbol: ")
console.log(arbol);
